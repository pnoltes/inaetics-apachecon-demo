SET(
	BUNDLE_NAME "simulator"
)

SET(
	BUNDLE_SYMBOLICNAME  "org.inaetics.apachecon.simulator"
)

SET(BUNDLE_VERSION "0.0.1")

include_directories(
	"private/include"
	"public/include"
	"${PROJECT_SOURCE_DIR}/utils/include"
)

bundle(simulator
	SOURCES
		private/src/bundle_activator
		private/src/simulatorCmp
		${PROJECT_SOURCE_DIR}/utils/src/locationUtils
	FILES
		${PROJECT_SOURCE_DIR}/api/org.inaetics.apachecon.api.provider.TargetProvider.descriptor
)


SET(BUNDLE_LIB "simulator")

target_link_libraries(${BUNDLE_LIB} ${CELIX_FRAMEWORK_LIBRARY} ${CELIX_UTILS_LIBRARY})

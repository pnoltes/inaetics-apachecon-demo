
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <signal.h>
#include <unistd.h>

#include <celix_errno.h>
#include <stdio.h>

#include "target.h"
#include "locationUtils.h"
#include "simulatorCmp.h"

#define MAX_TARGETS 	50
#define TIMEOUT_IN_MS	200
#define SOURCE			"simulated"

static celix_status_t spawnTarget(simulatorCmp_pt comp, long updateTime, target_pt* target);
static celix_status_t moveTarget(target_pt target, long forTime);
static void* updateTargets(void* handle);

static struct location demoCenter = { .lat = 47.502657, .lon = 19.066798};
static double const maxRange = 700;

struct simulatorCmp {
    long nextId;
	struct target_sequence *targets;
	pthread_t thread;
	pthread_mutex_t mutex; //projects targets
	bool running;
};

celix_status_t simulatorCmp_create(simulatorCmp_pt *out) {
	celix_status_t status = CELIX_SUCCESS;

    simulatorCmp_pt component = calloc(1, sizeof(*component));

	if (component != NULL) {
		component->running = false;

		component->nextId =1;
		component->targets = calloc(1, sizeof(struct target_sequence)); //initialized on len 0.
		pthread_mutex_init(&component->mutex, NULL);

        //seeding random
        struct timespec ts_rand;
        clock_gettime(CLOCK_REALTIME, &ts_rand);
        srand(ts_rand.tv_nsec);

        *out = component;
    } else {
		status = CELIX_ENOMEM;
	}
	return status;
}

celix_status_t simulatorCmp_destroy(simulatorCmp_pt component) {
	celix_status_t status = CELIX_SUCCESS;
	if (component != NULL) {
		//TODO free targets
		//TODO lock
		free(component->targets);
		free(component);
	} else {
		status = CELIX_ILLEGAL_ARGUMENT;
	}
	return status;
}

celix_status_t simulatorCmp_start(simulatorCmp_pt component) {
	celix_status_t status = CELIX_SUCCESS;

	component->running = true;

	pthread_create(&component->thread, NULL, updateTargets, component);

	return status;
}

celix_status_t simulatorCmp_stop(simulatorCmp_pt component) {
	celix_status_t status = CELIX_SUCCESS;

	component->running = false;
    pthread_kill(component->thread, SIGUSR1);
	pthread_join(component->thread, NULL);

	return status;
}

static void* updateTargets(void* handle) {
	simulatorCmp_pt component = (simulatorCmp_pt) handle;
	unsigned int i;

    while (component->running) {
        long updateTime = time(NULL);

        struct target_sequence *newSeq = calloc(1, sizeof(*newSeq));
        newSeq->buf = calloc(MAX_TARGETS, sizeof(struct target *));
        newSeq->cap = MAX_TARGETS;
        newSeq->len = 0;

        pthread_mutex_lock(&component->mutex);
        for (i = 0; i < component->targets->len; i += 1) {
            target_pt target = component->targets->buf[i];
            moveTarget(target, updateTime);

            double dist = locationUtils_distance(&target->location, &demoCenter);
            if (dist <= maxRange) {
                //printf("moved target %li\n", target->id);
                printf("Moved target %li from (%f,%f) -> (%f,%f). distance is %f\n", target->id, target->location.lat, target->location.lon, demoCenter.lat, demoCenter.lon, dist);
                target->lastUpdated = updateTime;
                newSeq->buf[newSeq->len++] = target;
            } else {
                //free(target->source);
                free(target);
                printf("removed target %li, distance was %f\n", target->id, dist);
            }
        }


        if (newSeq->len < MAX_TARGETS) {
            target_pt target = NULL;
            int diff = MAX_TARGETS - newSeq->len;
            for (i = 0; i < diff; i += 1) {
                target = NULL;
                spawnTarget(component, updateTime, &target);
                printf("spawned target %li at (%f,%f)\n", target->id, target->location.lat, target->location.lon);
                newSeq->buf[newSeq->len++] = target;
            }
        }


        struct target_sequence *oldSeq = component->targets;
        component->targets = newSeq;
        free(oldSeq);
        pthread_mutex_unlock(&component->mutex);
        usleep(TIMEOUT_IN_MS * 1000);
    }
}

static double generateRandom() {
    return (double) (((double) rand()) / ((double) RAND_MAX));
}

static celix_status_t spawnTarget(simulatorCmp_pt comp, long updateTime, target_pt* target) {
	celix_status_t status = CELIX_SUCCESS;

	struct location loc = locationUtils_move(&demoCenter, generateRandom() * 360, generateRandom() * maxRange);
    double speed = generateRandom() * 5 + 1;
	double heading = generateRandom() * 360;

	*target = calloc(1, sizeof(**target));

	if (*target) {
		(*target)->id = comp->nextId++;
		(*target)->lastUpdated = updateTime;
		(*target)->location= loc;
		(*target)->speed = speed;
		(*target)->heading = heading;
        //(*target)->source = strdup(SOURCE);
	} else {
		status = CELIX_ENOMEM;
	}
	return status;
}

static celix_status_t moveTarget(target_pt target, long forTime) {
	celix_status_t status = CELIX_SUCCESS;

	long t1 = target->lastUpdated;
	long t2 = forTime;
	double elapsed = t2 - t1;
	elapsed = elapsed > 0 ? elapsed : 0;
	//elapsed = elapsed / 1000; //in second

	double dist = target->speed * elapsed;

	target->location = locationUtils_move(&target->location, target->heading, dist);

	return status;
}

celix_status_t simulatorCmp_getTargets(simulatorCmp_pt component, struct target_sequence **targets) {
    int status = CELIX_SUCCESS;

    /*
    struct target_sequence *copy = calloc(1, sizeof(*copy));
    *targets = copy;
    return status;
     */

    pthread_mutex_lock(&component->mutex);
    struct target_sequence *copy = calloc(1, sizeof(*copy));
    copy->buf = calloc(component->targets->len, sizeof(struct target *));
    copy->cap = component->targets->len;
    int i;
    for (i = 0; i < copy->cap; i += 1) {
        struct target *tCopy = calloc(1, sizeof(struct target));
        struct target *target = component->targets->buf[i];
        tCopy->id = target->id;
        tCopy->heading = target->heading;
        tCopy->lastUpdated = target->lastUpdated;
        tCopy->location = target->location;
        tCopy->speed = target->speed;
        copy->buf[copy->len++] = tCopy;
    }
    pthread_mutex_unlock(&component->mutex);
    (*targets) = copy;

    /*
    for (i = 0; i < copy->len; i += 1) {
        printf("got target nr %i with speed %f\n", i, copy->buf[i]->speed);
    }
     */

    return status;
}

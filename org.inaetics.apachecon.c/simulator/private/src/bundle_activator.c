

#include <stdlib.h>
#include <bundle_context.h>

#include "simulatorCmp.h"

struct activator {
	bundle_context_pt context;
	simulatorCmp_pt simulator;

    struct target_provider provider;
    service_registration_pt  registration;
};

typedef struct activator* activator_pt;

celix_status_t bundleActivator_create(bundle_context_pt context, void **userData) {
	celix_status_t status = CELIX_SUCCESS;

	*userData = calloc(1, sizeof(struct activator));

	if (*userData) {
		((activator_pt) *userData)->context = context;
		((activator_pt) *userData)->simulator = NULL;
	} else {
		status = CELIX_ENOMEM;
	}
	return status;
}

celix_status_t bundleActivator_start(void * userData, bundle_context_pt context) {
	celix_status_t status = CELIX_SUCCESS;
	activator_pt activator = (activator_pt) userData;

	status = simulatorCmp_create(&activator->simulator);

    if (status == CELIX_SUCCESS) {
        simulatorCmp_start(activator->simulator);
    }

    properties_pt props = properties_create();
    properties_set(props, "source", "simulated");
    properties_set(props, "service.exported.interfaces", TARGET_PROVIDER_SERVICE_NAME);
    activator->provider.handle = activator->simulator;
    activator->provider.getTargets = (void *)simulatorCmp_getTargets;
    bundleContext_registerService(context, TARGET_PROVIDER_SERVICE_NAME, &activator->provider, props, &activator->registration);

	return status;
}


celix_status_t bundleActivator_stop(void * userData, bundle_context_pt context) {
    printf("Sensor in simulator stop\n");
	celix_status_t status = CELIX_SUCCESS;
	activator_pt activator = (activator_pt) userData;

    serviceRegistration_unregister(activator->registration);

	if (activator->simulator != NULL) {
		status = simulatorCmp_stop(activator->simulator);
		if (status == CELIX_SUCCESS) {
			status = simulatorCmp_destroy(activator->simulator);
		}
	}

	return status;
}

celix_status_t bundleActivator_destroy(void * userData, bundle_context_pt context) {
	celix_status_t status = CELIX_SUCCESS;
	activator_pt activator = (activator_pt) userData;

	free(activator);

	return status;
}


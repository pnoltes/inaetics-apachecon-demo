#ifndef __SIMULATORCMP_H_
#define __SIMULATORCMP_H_

#include "target_provider.h"

typedef struct simulatorCmp *simulatorCmp_pt;

celix_status_t simulatorCmp_create(simulatorCmp_pt *component);
celix_status_t simulatorCmp_start(simulatorCmp_pt component);
celix_status_t simulatorCmp_stop(simulatorCmp_pt component);
celix_status_t simulatorCmp_destroy(simulatorCmp_pt component);

celix_status_t simulatorCmp_getTargets(simulatorCmp_pt component, struct target_sequence **targets);

#endif //__SIMULATORCMP_H_

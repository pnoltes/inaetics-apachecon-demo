

#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>

//Component Struct
#include "sensorCmp.h"
#include "sensor.h"
#include "target_provider.h"
#include "detection_provider.h"
#include "locationUtils.h"

#define TIMEOUT_IN_MS 200

struct sensorCmp {
	struct target_provider * provider;
	pthread_mutex_t mutex_for_provider;
	struct location center;
	double range;
	char *name;

    long nextId;
    struct detection_sequence *detections;
    pthread_mutex_t mutex; //lock for detections

    pthread_t thread;
    volatile bool running;
};


static struct detection_sequence *sensorCmp_copyDetectionSequence(struct detection_sequence *pSequence);
static void * sensorCmp_thread(void *data);

//Create function
celix_status_t sensorCmp_create(struct location loc, double range, char *name, sensorCmp_pt *out) {
	celix_status_t status = CELIX_SUCCESS;
	sensorCmp_pt component = calloc(1, sizeof(*component));
	if (component != NULL) {
        component->nextId = 1;
		component->provider = NULL;
		pthread_mutex_init(&component->mutex_for_provider, NULL);
		component->center = loc;
		component->range = range;
		component->name = strdup(name);
        pthread_mutex_init(&component->mutex, NULL);
        component->running = false;
        component->detections  = calloc(1, sizeof(struct detection_sequence));
		(*out) = component;
	} else {
		status = CELIX_ENOMEM;
	}	
	return status;
}


//Destroy function
celix_status_t sensorCmp_destroy(sensorCmp_pt component) {
	celix_status_t status = CELIX_SUCCESS;
	if (component != NULL) {
		free(component);
        //TODO delete detections
	} else {
		status = CELIX_ILLEGAL_ARGUMENT;
	}
	return status;
}

//Start function
celix_status_t sensorCmp_start(sensorCmp_pt component) {
	celix_status_t status = CELIX_SUCCESS;
    component->running = true;
    pthread_create(&component->thread, NULL, sensorCmp_thread, component);
	return status;
}

//Stop function
celix_status_t sensorCmp_stop(sensorCmp_pt component) {
	celix_status_t status = CELIX_SUCCESS;
    printf("stopping thread\n");
    component->running = false;
    pthread_kill(component->thread, SIGUSR1);
    pthread_join(component->thread, NULL);
	return status;
}

static void * sensorCmp_thread(void *data) {
    sensorCmp_pt component = data;
    int i;

    while (component->running) {

        //receive targets
        struct target_sequence *targets = NULL;
        pthread_mutex_lock(&component->mutex_for_provider);
        if (component->provider != NULL) {
            int rc = component->provider->getTargets(component->provider->handle, &targets);
            if (rc != 0) {
                fprintf(stderr, "Error invoking target provider %p\n", component->provider);
            }
        }
        pthread_mutex_unlock(&component->mutex_for_provider);

        //create detections
        size_t nrOfTargets = targets != NULL ? targets->len : 0;
        struct detection_sequence *seq = calloc(1,sizeof(*seq));
        seq->buf = calloc(nrOfTargets, sizeof(struct detection));
        seq->cap = nrOfTargets;

        //find target within range
        for (i = 0; targets != NULL && i < targets->len; i += 1) {
            double dist = locationUtils_distance(&targets->buf[i]->location, &component->center);
            if (dist <= component->range) {
                struct detection *detection = calloc(1, sizeof(*detection));
                detection->center = component->center;
                detection->id = component->nextId++;
                detection->range = dist;
                detection->simulated = *targets->buf[i];
                seq->buf[seq->len++] = detection;
                //printf("detection within range %f\n", dist);
            }
            free(targets->buf[i]);
        }
        //printf("Sensor %s: got %i detections out of %i targets\n", component->name, seq->len, nrOfTargets);
        free(targets);

        //swap detections
        pthread_mutex_lock(&component->mutex);
        struct detection_sequence *old = component->detections;
        component->detections = seq;
        for (i = 0; i < old->len; i += 1) {
            free(old->buf[i]);
        }
        free(old);
        pthread_mutex_unlock(&component->mutex);

        usleep(TIMEOUT_IN_MS * 1000);
    }

    return NULL;
}

static struct detection_sequence *sensorCmp_copyDetectionSequence(struct detection_sequence *input) {
    struct detection_sequence *seq = calloc(1, sizeof(*seq));
    seq->buf = calloc(input->len, sizeof(*seq->buf));
    seq->len = seq->cap = input->len;
    int i;
    for (i = 0; i < seq->len; i += 1) {
        seq->buf[i] = calloc(1, sizeof(*seq->buf[i]));
        memcpy(seq->buf[i], input->buf[i], sizeof(struct detection));

        //printf("speed is %f\n", seq->buf[i]->simulated.speed);
    }
    return seq;
}


celix_status_t sensorCmp_getDetections(sensorCmp_pt component, struct detection_sequence **out) {
    celix_status_t status = CELIX_SUCCESS;
    pthread_mutex_lock(&component->mutex);
    struct detection_sequence *seq = sensorCmp_copyDetectionSequence(component->detections);
    *out = seq;
    pthread_mutex_unlock(&component->mutex);
    return status;
}

celix_status_t sensorCmp_getLocation(sensorCmp_pt component, struct location *loc) {
	celix_status_t status = CELIX_SUCCESS;
	loc->lat = component->center.lat;
	loc->lon = component->center.lon;
	return status;
}

celix_status_t sensorCmp_getName(sensorCmp_pt component, char **name) {
	celix_status_t status = CELIX_SUCCESS;
	*name = strdup(component->name);
	return status;
}

celix_status_t sensorCmp_getRange(sensorCmp_pt component, double *range) {
	celix_status_t status = CELIX_SUCCESS;
	*range = component->range;
	return status;
}

celix_status_t sensorCmp_set_provider(sensorCmp_pt component, struct target_provider *provider) {
	celix_status_t status = CELIX_SUCCESS;
    pthread_mutex_lock(&component->mutex_for_provider);
	component->provider = provider;
	pthread_mutex_unlock(&component->mutex_for_provider);
	return status;
}


//TODO update fields from <service>Type to <service>For<component>Type
//TODO improve names to camel case (e.g. from _add_logger_for_example to _addLoggerToExample)
#include <stdlib.h>

#include <pthread.h>

#include <constants.h>
#include <bundle_context.h>
#include <service_tracker.h>
#include <hash_map.h>
#include <string.h>
#include <locationUtils.h>

#include "sensorCmp.h"
#include "target_provider.h"
#include "sensor.h"
#include "detection_provider.h"

static celix_status_t bundleActivator_add_provider_for_sensorCmp(void *handle, service_reference_pt ref, void *service);
static celix_status_t bundleActivator_remove_provider_for_sensorCmp(void *handle, service_reference_pt ref, void *service);

struct activator {
	bundle_context_pt context;
	service_tracker_customizer_pt providerCustomizer;
	service_tracker_pt providerServiceTracker;
	size_t nrOfSensors;

	array_list_pt sensorComponents;
	//TODO mutex

	struct target_provider *current_provider_service_for_sensorCmp;
	struct sensor *sensorServices;
	service_registration_pt  *sensorRegistrations;
	struct detection_provider *detectionProviders;
	service_registration_pt  *detectionProviderRegistrations;
};

static double groupRange = 150;
static double groupWidth = 400;
static double groupHeight = 400;
static struct location demoCenter = { .lat = 47.502657, .lon = 19.066798};


static void createSensors(struct activator *act, bundle_context_pt context, int groupNr) {
	struct location topLeft = groupNr == 1 ?
        locationUtils_moveDelta(&demoCenter, -400, -200) :
          locationUtils_moveDelta(&demoCenter, 0, -200);
	int row = 0;
	int column =0;
	double deltaX, deltaY;
	int index = 1;
	while(1) {
		deltaY = (groupRange / 2) + row * groupRange;
        printf("deltaY is %f\n", deltaY);
		if (deltaY > (groupHeight + (groupRange/2))) {
			break;
		}
		while (1) {
			deltaX = (groupRange / 2) + column * groupRange;
            printf("deltaX is %f\n", deltaX);
			if (deltaX > (groupWidth + (groupRange/2))) {
                column = 0;
				break;
			}
			struct location center = locationUtils_moveDelta(&topLeft, deltaX, deltaY);
			sensorCmp_pt cmp = NULL;
			char name[32];
			snprintf(name, 32, "Sensor group %i - %i", groupNr, index++);
			int rc = sensorCmp_create(center, groupRange, name, &cmp);
			arrayList_add(act->sensorComponents, cmp);
            column += 1;
		}
        row += 1;
	}
}

static void createSensor(struct activator *act, bundle_context_pt context) {
    char *name = NULL;
    char *rangeStr = NULL;
    char *latStr = NULL;
	char *lonStr = NULL;

    bundleContext_getProperty(context, "sensor.name", &name);
    bundleContext_getProperty(context, "sensor.range", &rangeStr);
    bundleContext_getProperty(context, "sensor.location.lat", &latStr);
	bundleContext_getProperty(context, "sensor.location.lon", &lonStr);

    name = name == NULL ?  "Celix Sensor" : name;

	double range = rangeStr == NULL ? 0 : strtod(rangeStr, NULL);
    range = range == 0 ? 250 : range;

	double lat = latStr == NULL ? 0 : strtod(latStr, NULL);
	lat = lat == 0 ? 44.5403 : lat;

	double lon = latStr == NULL ? 0 : strtod(lonStr, NULL);
	lon = lon == 0 ? -78.5463 : lon;

	struct location loc;
	loc.lat = lat;
	loc.lon = lon;
	sensorCmp_pt cmp = NULL;
	printf("Creating sensor at (%f,%f) with range %f\n", loc.lat, loc.lon, range);
	int rc = sensorCmp_create(loc, range, name, &cmp);
	arrayList_add(act->sensorComponents, cmp);
}


celix_status_t bundleActivator_create(bundle_context_pt context, void **userData) {
	celix_status_t status = CELIX_SUCCESS;
	struct activator *activator = calloc(1, sizeof(struct activator));
	if (activator != NULL) {
		(*userData) = activator;
		arrayList_create(&activator->sensorComponents);

		char *id = NULL;
		bundleContext_getProperty(context, "deployment_admin_identification", &id);
		if (id == NULL) {
			createSensor(activator, context);
		} else if (strcmp(id, "celix-sensor-group-1") == 0) {
			createSensors(activator, context, 1);
		} else if (strcmp(id, "celix-sensor-group-2") == 0) {
			createSensors(activator, context, 2);
		} else {
			createSensor(activator, context);
		}


		activator->providerServiceTracker = NULL;
		serviceTrackerCustomizer_create(activator, NULL, bundleActivator_add_provider_for_sensorCmp, NULL, bundleActivator_remove_provider_for_sensorCmp, &activator->providerCustomizer);
		//serviceTracker_createWithFilter(context, "(&(objectClass=org.ianetics.apachecon.api.provider.TargetProvider)(source=simulated))", activator->providerCustomizer, &activator->providerServiceTracker);
        serviceTracker_create(context, TARGET_PROVIDER_SERVICE_NAME, activator->providerCustomizer, &activator->providerServiceTracker);

		activator->nrOfSensors = arrayList_size(activator->sensorComponents);
		activator->sensorServices = calloc(activator->nrOfSensors, sizeof(struct sensor));
		activator->sensorRegistrations = calloc(activator->nrOfSensors, sizeof(service_registration_pt));
		activator->detectionProviders = calloc(activator->nrOfSensors, sizeof(struct detection_provider));
		activator->detectionProviderRegistrations = calloc(activator->nrOfSensors, sizeof(service_registration_pt));
	} else {
		status = CELIX_ENOMEM;
	}
	return status;
}


celix_status_t bundleActivator_start(void *userData, bundle_context_pt context) {
	celix_status_t status = CELIX_SUCCESS;
	struct activator *activator = userData;

	serviceTracker_open(activator->providerServiceTracker);

	int i;
	for (i = 0; i < activator->nrOfSensors; i += 1) {
		sensorCmp_pt cmp = arrayList_get(activator->sensorComponents, i);

		activator->sensorServices[i].handle = cmp;
		activator->sensorServices[i].getLocation = (void *) sensorCmp_getLocation;
		activator->sensorServices[i].getName = (void *) sensorCmp_getName;
		activator->sensorServices[i].getRange = (void *) sensorCmp_getRange;

		activator->detectionProviders[i].handle = cmp;
		activator->detectionProviders[i].getDetections = (void *) sensorCmp_getDetections;

		sensorCmp_start(cmp);

		{
			properties_pt props = properties_create();
			properties_set(props, "service.exported.interfaces", SENSOR_SERVICE_NAME);
			bundleContext_registerService(context, (char *)SENSOR_SERVICE_NAME, &activator->sensorServices[i], props, &activator->sensorRegistrations[i]);
		}

		{
			properties_pt props = properties_create();
			properties_set(props, "service.exported.interfaces", DETECTION_PROVIDER_SERVICE_NAME);
			bundleContext_registerService(context, (char *)DETECTION_PROVIDER_SERVICE_NAME, &activator->detectionProviders[i], props, &activator->detectionProviderRegistrations[i]);
		}

	}

	return status;
}

celix_status_t bundleActivator_stop(void *userData, bundle_context_pt context) {
    printf("Sensor in activator stop\n");
	celix_status_t status = CELIX_SUCCESS;
	struct activator *activator = userData;

	int i;
	for (i = 0; i < activator->nrOfSensors; i += 1) {
		sensorCmp_pt cmp = arrayList_get(activator->sensorComponents, i);

		serviceRegistration_unregister(activator->sensorRegistrations[i]);
		serviceRegistration_unregister(activator->detectionProviderRegistrations[i]);

		sensorCmp_stop(cmp);
	}

	serviceTracker_close(activator->providerServiceTracker);

	return status;
}

celix_status_t bundleActivator_destroy(void *userData, bundle_context_pt context) {
	celix_status_t status = CELIX_SUCCESS;
	struct activator *activator = userData;

	int i;
	for (i = 0; i < activator->nrOfSensors; i += 1) {
		sensorCmp_pt cmp = arrayList_get(activator->sensorComponents, i);
		sensorCmp_destroy(cmp);
	}

	arrayList_destroy(activator->sensorComponents);
	free(activator->sensorServices);
	free(activator->sensorRegistrations);
	free(activator->detectionProviders);
	free(activator->detectionProviderRegistrations);
	free(activator);

	return status;
}

//Add/Remove functions for the trakkers
static celix_status_t bundleActivator_add_provider_for_sensorCmp(void *handle, service_reference_pt ref, void *service) {
	celix_status_t status = CELIX_SUCCESS;
    struct activator *act = handle;
    act->current_provider_service_for_sensorCmp = service;
    int i;
    for (i = 0; i < act->nrOfSensors; i += 1) {
        sensorCmp_pt cmp = arrayList_get(act->sensorComponents, i);
        sensorCmp_set_provider(cmp, (struct target_provider *) service);
    }
    return status;
}

static celix_status_t bundleActivator_remove_provider_for_sensorCmp(void *handle, service_reference_pt ref, void *service) {
	celix_status_t status = CELIX_SUCCESS;
    struct activator *act = handle;
    int i;
    for (i = 0; i < act->nrOfSensors; i += 1) {
        sensorCmp_pt cmp = arrayList_get(act->sensorComponents, i);
        sensorCmp_set_provider(cmp, NULL);

    }
    act->current_provider_service_for_sensorCmp = NULL;
	return status;
}


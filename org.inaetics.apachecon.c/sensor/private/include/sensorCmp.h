#ifndef __SENSORCMP_H_
#define __SENSORCMP_H_

#include "target_provider.h"
#include "detection_provider.h"


#include <celix_errno.h>
#include <array_list.h>

#include "sensor.h"

typedef struct sensorCmp *sensorCmp_pt;

celix_status_t sensorCmp_create(struct location loc, double range, char *name, sensorCmp_pt *component);
celix_status_t sensorCmp_start(sensorCmp_pt component);
celix_status_t sensorCmp_stop(sensorCmp_pt component);
celix_status_t sensorCmp_destroy(sensorCmp_pt component);

//sensor api
celix_status_t sensorCmp_getLocation(sensorCmp_pt component, struct location *loc);
celix_status_t sensorCmp_getName(sensorCmp_pt component, char **name);
celix_status_t sensorCmp_getRange(sensorCmp_pt component, double *range);

//detection provider api
celix_status_t  sensorCmp_getDetections(sensorCmp_pt component, struct detection_sequence **seq);

celix_status_t sensorCmp_set_provider(sensorCmp_pt component, struct target_provider * provider);

#endif //__SENSORCMP_H_

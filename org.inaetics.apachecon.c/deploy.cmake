deploy("apachecon-demonstrator-all" BUNDLES
        ${CELIX_BUNDLES_DIR}/shell.zip
        ${CELIX_BUNDLES_DIR}/shell_tui.zip
        sensor
		simulator
)

deploy("apachecon-demonstrator-sensor" BUNDLES
		${CELIX_BUNDLES_DIR}/shell.zip
		${CELIX_BUNDLES_DIR}/shell_tui.zip
		${CELIX_BUNDLES_DIR}/discovery_etcd.zip
		${CELIX_BUNDLES_DIR}/topology_manager.zip
		${CELIX_BUNDLES_DIR}/remote_service_admin_dfi.zip
		sensor
       PROPERTIES
            DISCOVERY_CFG_POLL_INTERVAL=1
            #LOGHELPER_ENABLE_STDOUT_FALLBACK=true
            RSA_PORT=50801
			DISCOVERY_ETCD_ROOT_PATH=inaetics/discovery
)

deploy("apachecon-demonstrator-sensor-group-1" BUNDLES
        ${CELIX_BUNDLES_DIR}/shell.zip
        ${CELIX_BUNDLES_DIR}/shell_tui.zip
        ${CELIX_BUNDLES_DIR}/discovery_etcd.zip
        ${CELIX_BUNDLES_DIR}/topology_manager.zip
        ${CELIX_BUNDLES_DIR}/remote_service_admin_dfi.zip
        sensor
        PROPERTIES
        DISCOVERY_CFG_POLL_INTERVAL=1
        #LOGHELPER_ENABLE_STDOUT_FALLBACK=true
        RSA_PORT=50801
        DISCOVERY_ETCD_ROOT_PATH=inaetics/discovery
        deployment_admin_identification=celix-sensor-group-1
)

deploy("apachecon-demonstrator-sensor-group-2" BUNDLES
        ${CELIX_BUNDLES_DIR}/shell.zip
        ${CELIX_BUNDLES_DIR}/shell_tui.zip
        ${CELIX_BUNDLES_DIR}/discovery_etcd.zip
        ${CELIX_BUNDLES_DIR}/topology_manager.zip
        ${CELIX_BUNDLES_DIR}/remote_service_admin_dfi.zip
        sensor
        PROPERTIES
        DISCOVERY_CFG_POLL_INTERVAL=1
        #LOGHELPER_ENABLE_STDOUT_FALLBACK=true
        RSA_PORT=50801
        DISCOVERY_ETCD_ROOT_PATH=inaetics/discovery
        deployment_admin_identification=celix-sensor-group-2
)

deploy("apachecon-demonstrator-simulator" BUNDLES
        ${CELIX_BUNDLES_DIR}/shell.zip
        ${CELIX_BUNDLES_DIR}/shell_tui.zip
        ${CELIX_BUNDLES_DIR}/discovery_etcd.zip
        ${CELIX_BUNDLES_DIR}/topology_manager.zip
        ${CELIX_BUNDLES_DIR}/remote_service_admin_dfi.zip
        simulator
        PROPERTIES
            DISCOVERY_CFG_POLL_INTERVAL=1
            #LOGHELPER_ENABLE_STDOUT_FALLBACK=true
            RSA_PORT=50802
            DISCOVERY_ETCD_ROOT_PATH=inaetics/discovery
)


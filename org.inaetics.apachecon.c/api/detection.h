#ifndef __APACHECON_API__DETECTION_H_
#define __APACHECON_API__DETECTION_H_

#include "location.h"
#include "target.h"

struct detection {
    int64_t id;
	struct location center;
	double range;
	struct target simulated;
};

typedef struct detection* detection_pt;


#endif

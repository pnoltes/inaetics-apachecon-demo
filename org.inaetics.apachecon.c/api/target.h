#ifndef __APACHECON_API__TARGET_H_
#define __APACHECON_API__TARGET_H_

#include "location.h"

struct target {
	int64_t id;
	//const char *source;
	struct location location;
	double speed;
	double heading;
	int64_t lastUpdated;
};

typedef struct target* target_pt;


#endif

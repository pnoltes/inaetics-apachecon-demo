#ifndef __APACHECON_API__SENSOR_H_
#define __APACHECON_API__SENSOR_H_

#include <stdint.h>
#include <stdbool.h>

#include "location.h"

#define SENSOR_SERVICE_NAME "org.inaetics.apachecon.api.sensor.Sensor"

struct sensor {
	void *handle;
    int (*getName)(void *handle, char **name);
    int (*getLocation)(void *handle, struct location *loc);
    int (*getRange)(void *handle, double *range);
};

#endif

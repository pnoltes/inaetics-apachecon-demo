#ifndef __APACHECON_API__DETECTION_PROVIDER_H_
#define __APACHECON_API__DETECTION_PROVIDER_H_

#include <stdint.h>
#include <stdbool.h>

#include "detection.h"

#define DETECTION_PROVIDER_SERVICE_NAME "org.inaetics.apachecon.api.provider.DetectionProvider"

struct detection_sequence {
	uint32_t cap;
	uint32_t len;
	struct detection **buf;
};

struct detection_provider {
	void *handle;
	int (*getDetections)(void *handle, struct detection_sequence **seq);
};

#endif

#ifndef __APACHECON_API__TARGET_PROVIDER_H_
#define __APACHECON_API__TARGET_PROVIDER_H_

#include <stdint.h>
#include <stdbool.h>

#define TARGET_PROVIDER_SERVICE_NAME "org.inaetics.apachecon.api.provider.TargetProvider"

struct target_sequence {
	uint32_t cap;
	uint32_t len;
	struct target **buf;
};

struct target_provider {
	void *handle;
	int (*getTargets)(void *handle, struct target_sequence **seq);
};

#endif

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include "location.h"
#include "locationUtils.h"

static double toDegrees(double angleRad) {
	return angleRad * 180 / M_PI;
}

static double toRadians(double degreesAngle) {
	return degreesAngle * M_PI / 180;
}

double locationUtils_distance(struct location* loc1, struct location* loc2) {
    double theta = toRadians(loc1->lon - loc2->lon);
    double lat1 = toRadians(loc1->lat);
    double lat2 = toRadians(loc2->lat);

    double dist = sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(theta);
    dist = acos(dist);
    dist = toDegrees(dist);
    dist = dist * 60 * 1.1515 * 1.609344 * 1000;

    return dist;
}


struct location locationUtils_move(struct location* start, double heading, double dist) {

    //printf("moving (%f,%f) at angle %f distance %f\n", start->lat, start->lon, heading, dist);
    double lat1 = toRadians(start->lat);
    double lon1 = toRadians(start->lon);
    double angle = toRadians(heading);

    double dist_radians = toRadians(dist / (60 * 1.1515 * 1.609344 * 1000));
    double lat = asin(sin(lat1) * cos(dist_radians) + cos(lat1) * sin(dist_radians) * cos(angle));
    double dlon = atan2(sin(angle) * sin(dist_radians) * cos(lat1), cos(dist_radians) - sin(lat1) * sin(lat));
	double lon = fmod((lon1-dlon + M_PI), (M_PI*2)) - M_PI;

	struct location result;

    result.lat = toDegrees(lat);
    result.lon = toDegrees(lon);

	return result;
}


struct location locationUtils_moveDelta(struct location* start, double deltaX, double deltaY) {
	//double angle = atan2(deltaY, deltaX); // In radians
    double angle = atan2(-1 * deltaX, -1 * deltaY); // In radians
	double dist = sqrt(deltaX*deltaX + deltaY*deltaY);

	return locationUtils_move(start, toDegrees(angle), dist);
}

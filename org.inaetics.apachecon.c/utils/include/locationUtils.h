#ifndef __LOCATIONUTILS_H_
#define __LOCATIONUTILS_H_

#include "location.h"

double locationUtils_distance(struct location* loc1, struct location* loc2);
struct location locationUtils_move(struct location* start, double heading, double dist);
struct location locationUtils_moveDelta(struct location* start, double deltaX, double deltaY);

#endif

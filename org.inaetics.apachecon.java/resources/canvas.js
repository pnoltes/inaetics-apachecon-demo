	var map;
 	var sensors = [];
 	var deArr = [];
 	var simArr = [];
 	var targetsArr = [];
 	
 	var showSensors = true;
 	var showDetections = true;
 	var showSimulated = false;
 	var showTargets = true;

    var canvas;
    var ctx; 
    var center;

    var width = 1000;
    var height = 500;
 	
 	function initMap() {
        canvas = document.getElementById("map");
        ctx = canvas.getContext("2d");
        center = {lat:47.502657, lon:19.066798};

        ctx.translate(500, 250);
        ctx.scale(0.6, 0.6);
 	}

    function draw() {
        ctx.clearRect(-width,-height,width*2,height*2);

        var i;
        for (i = 0; showSensors && i<sensors.length; i += 1) {
            var c = delta(center, sensors[i].location);
            var range = sensors[i].range;
            ctx.beginPath();
            ctx.fillStyle = "rgba(150, 150, 150, 0.6)";
            ctx.arc(c.x, c.y, range, 0, 2 * Math.PI);
            ctx.fill();
        }

        for (i = 0; showDetections && i<deArr.length; i +=1) {
            var c = delta(center, deArr[i].center);
            console.log("drawing detction at " + c.x + ", " + c.y);
            var range = deArr[i].range;
            console.log("detection range is " + range);
            ctx.beginPath();
            ctx.strokeStyle = '#ff0000';
            ctx.lineWidth = 4;
            ctx.arc(c.x, c.y, range, 0, 2 * Math.PI);
            ctx.stroke();
        }

        for (i = 0; showTargets && i<targetsArr.length; i += 1) {
            var c =  delta(center, targetsArr[i].location);
            ctx.strokeStyle = '#000000';
            ctx.lineWidth = 4;
            ctx.strokeRect(c.x - 8, c.y - 8,16,16);
            var next = delta(center, targetsArr[i].prediction);
            ctx.beginPath();
            ctx.moveTo(c.x, c.y);
            ctx.lineTo(next.x, next.y);
            ctx.stroke();
        }

        setTimeout(function() { draw(); }, 1000);
    }
 	
 	function processSensors() {
 		$.getJSON("/sensors", function(local_sensors) {
 			sensors = local_sensors
 		}).fail(function() {
            sensors = []
        });
 		setTimeout(function() { processSensors(); }, 1000);
 	}
 	
 	function processDetections() {
 		$.getJSON("/detections", function(detections) {
            deArr = detections;
        }).fail(function() {
            deArr = [];
        });
 		setTimeout(function() { processDetections(); }, 1000);
 	}
 	
 	function processSimulated() {
 		$.getJSON("/simulated", function(targets) {
            simArr = targets;
        }).fail(function() {
            simArr = [];
        });
 		setTimeout(function() { processSimulated(); }, 1000);
 	}
 	
 	function processTargets() {
 		$.getJSON("/processed", function(targets) {
            targetsArr = targets;
        }).fail(function() {
            targetsArr = [];
        });
 		setTimeout(function() { processTargets(); }, 1000);
 	}
 
 
	$( document ).ready(function() {
	    console.log( "ready!" );
	    initMap();
	    
	    $('#sensorsCheck').click(function() {
	    	showSensors = $(this).is(':checked'); 
    	});
    	$('#detectionsCheck').click(function() {
	    	showDetections = $(this).is(':checked');
    	});
    	$('#simulatedCheck').click(function() {
	    	showSimulated = $(this).is(':checked');
    	});
    	$('#targetsCheck').click(function() {
	    	showTargets = $(this).is(':checked');
    	});
	    

        draw();
	    processSensors();
	    processDetections();
	    processSimulated();
	    processTargets();
	}); 
	

function toRadians(angle) {
    return (angle / 180) * Math.PI;
}

function toDegrees(angle) {
    return (angle / (Math.PI)) * 180;
}

function distance(loc1, loc2) {
	var theta = toRadians(loc1.lon - loc2.lon);
	var lat1 = toRadians(loc1.lat);
	var lat2 = toRadians(loc2.lat);
	
	var dist = Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(theta);
	dist = Math.acos(dist);
	dist = toDegrees(dist);
	dist = dist * 60 * 1.1515 * 1.609344 * 1000;

	return dist;
}

function move(start, heading, dist) {
    var lat1 = toRadians(start.lat);
    var lon1 = toRadians(start.lon);
    var angle = toRadians(heading);
    
    var dist_radians = toRadians(dist / (60 * 1.1515 * 1.609344 * 1000));
    var lat = Math.asin(Math.sin(lat1) * Math.cos(dist_radians) + Math.cos(lat1) * Math.sin(dist_radians) * Math.cos(angle));
    var dlon = Math.atan2(Math.sin(angle) * Math.sin(dist_radians) * Math.cos(lat1), Math.cos(dist_radians) - Math.sin(lat1) * Math.sin(lat));
    var lon = ((lon1-dlon + Math.PI) % (2*Math.PI)) - Math.PI;
    
    var result = {lat:toDegrees(lat), lon:toDegrees(lon)};
    return result;
}

function delta(relativeNullPoint, loc) {
    var deltaLatitude = loc.lat- relativeNullPoint.lat
    var deltaLongitude = loc.lon- relativeNullPoint.lon
    var latitudeCircumference = 40075160 * Math.cos(toRadians(relativeNullPoint.lat))
    var resultX = deltaLongitude * latitudeCircumference / 360
    var resultY = deltaLatitude * 40008000 / 360
    return {x:resultX, y:resultY};
}

function measure(loc1, loc2){ 
    var R = 6378.137; // Radius of earth in KM
    var dLat = (loc2.lat - loc1.lat) * Math.PI / 180;
    var dLon = (loc2.lon - loc1.lon) * Math.PI / 180;
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(loc1.lat * Math.PI / 180) * Math.cos(loc2.lat * Math.PI / 180) *
        Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return d * 1000; // meters
}
	
function moveDelta(start, deltaX, deltaY) {
	var angle = Math.atan2(-1 * deltaX, -1 * deltaY); // In radians
	var dist = Math.sqrt(deltaX*deltaX + deltaY*deltaY);
	return LocationUtils.move(start, toDegrees(angle), dist);
}

//tests
/*
$(document).ready(function() {
    console.log("testing utils");
    
    var loc1 = {lat:47.502683, lon:19.066809};
    var loc2 = {lat:47.501672, lon:19.066778};

    console.log("distance loc1 loc1 = " + distance(loc1, loc1));
    console.log("distance loc1 loc2 = " + distance(loc1, loc2));
    console.log("distance loc1 loc2 = " + measure(loc1, loc2));

    var moved = move(loc1, 45, 300);
    console.log("loc1 after move 45 degrees 300 m is " + moved.lat + ", " + moved.lon);

    var xy = delta(loc1, loc2);
    console.log("delta x,y is " + xy.x + ", " + xy.y);
});
*/

package org.inaetics.apachecon.sensor;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicLong;

import org.inaetics.apachecon.api.data.Detection;
import org.inaetics.apachecon.api.data.Location;
import org.inaetics.apachecon.api.data.Target;
import org.inaetics.apachecon.api.provider.DetectionProvider;
import org.inaetics.apachecon.api.provider.TargetProvider;
import org.inaetics.apachecon.api.sensor.Sensor;
import org.inaetics.apachecon.api.util.LocationUtils;

public class SensorImpl implements Sensor, DetectionProvider {
	
	private static final long TIMEOUT_IN_MS = 1000;
	
	private final List<TargetProvider> providers = new CopyOnWriteArrayList<TargetProvider>();
	private final String name;
	private final Location location;
	private final double range;
	private final Thread thread;
	private volatile List<Detection> detections = new LinkedList<Detection>();
	private AtomicLong nextId = new AtomicLong(1);
	

	public SensorImpl(String name, Location location, double range) {
		super();
		this.name = name;
		this.location = location;
		this.range = range;
		this.thread = new Thread(new Runnable() {	
			@Override
			public void run() {
				while (!Thread.interrupted()) {
					try {
						processDetections();
						Thread.sleep(TIMEOUT_IN_MS);
					} catch (InterruptedException e) {
						break;
					}
				}
			}
		}, "Sensor:"  + name);
	}
	
	public void addProvider(TargetProvider provider) {
		//System.out.println("target provider added for sensor");
		providers.add(provider);
	}
	
	public void removeProvider(TargetProvider provider) {
		providers.remove(provider);
	}
	
	public void start() {
		thread.start();
	}
	
	public void stop() {
		System.out.println("interrupting sensor processing");
		thread.interrupt();
	}
	
	public void processDetections() {
		List<Detection> updatedDetections = new LinkedList<Detection>();
		for (TargetProvider provider : providers) {
			List<Target> targets = null;
			try {
				targets = provider.getTargets();
			} catch (Exception e) {
				targets = new ArrayList<>();
				System.err.println("Error invoking TargetProvider");
			}
			for (Target target : targets) {		
				double dist = LocationUtils.distance(this.location, target.getLocation()); 
				if ( dist <= this.range) {
					Detection d = new Detection(nextId.getAndIncrement(), location, dist);
					d.setSimulated(target);
					updatedDetections.add(d);	
				}
			}
		}
		
		this.detections = updatedDetections;
	}
	
	@Override
	public List<Detection> getDetections() {
		return detections;
	}


	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public Location getLocation() {
		return this.location;
	}

	@Override
	public Double getRange() {
		return this.range;
	}
}

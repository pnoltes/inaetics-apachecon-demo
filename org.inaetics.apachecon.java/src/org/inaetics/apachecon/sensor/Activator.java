/**

 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.inaetics.apachecon.sensor;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.inaetics.apachecon.api.data.Location;
import org.inaetics.apachecon.api.provider.DetectionProvider;
import org.inaetics.apachecon.api.provider.TargetProvider;
import org.inaetics.apachecon.api.sensor.Sensor;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {
	
	private DependencyManager manager;

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        this.manager = manager;
    	
     
        String name = context.getProperty("sensor.name");
        name = name == null ? "Sensor" : name;
        
        String range = context.getProperty("sensor.range");
        range = range == null ? "250" : range;
        
        String lat = context.getProperty("sensor.lat");
        lat = lat == null ? "47.502657" : lat;
        
        String lon = context.getProperty("sensor.lot");
        lon = lon == null ? "19.066798" : lon;
        
        createSensor(Double.parseDouble(lat), Double.parseDouble(lon), Double.parseDouble(range), name);
    }

    
    public void createSensor(double lat, double lon, double range, String name) {
    	Properties props = new Properties();
    	props.put("service.exported.interfaces", new String[] {Sensor.class.getName(), DetectionProvider.class.getName()});
    	Sensor sensor = new SensorImpl(name, new Location(lat, lon), range);
    	manager.add(createComponent()
                .setInterface(new String[] {Sensor.class.getName(), DetectionProvider.class.getName()}, props)
                .setImplementation(sensor)
                .add(createServiceDependency().setService(TargetProvider.class)
                		.setCallbacks("addProvider", "removeProvider"))
        );
    }
    
    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {

    }

}

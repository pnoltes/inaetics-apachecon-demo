/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.inaetics.apachecon.processing;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.inaetics.apachecon.api.provider.DetectionProvider;
import org.inaetics.apachecon.api.provider.ProcessedTargetProvider;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        Properties props = new Properties();
        props.put("service.exported.interfaces", ProcessedTargetProvider.class.getName());
        
        manager.add(createComponent()
            .setInterface(ProcessedTargetProvider.class.getName(), props)
            .setImplementation(DetectionProcessing.class)
            .add(createServiceDependency().setService(DetectionProvider.class)
            		.setCallbacks("addDetectionProvider", "removeDetectionProvider"))
        );
    }
    
    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {

    }

}

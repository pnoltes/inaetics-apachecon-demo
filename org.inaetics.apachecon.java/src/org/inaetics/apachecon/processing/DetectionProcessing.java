package org.inaetics.apachecon.processing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.inaetics.apachecon.api.data.Detection;
import org.inaetics.apachecon.api.data.ProcessedTarget;
import org.inaetics.apachecon.api.data.Target;
import org.inaetics.apachecon.api.provider.DetectionProvider;
import org.inaetics.apachecon.api.provider.ProcessedTargetProvider;

public class DetectionProcessing implements ProcessedTargetProvider {
	
	private final int TIMEOUT_IN_MS = 1000;
	
	private final Map<Thread, DetectionProvider> providers = new ConcurrentHashMap<>();
	private final Map<Thread, Collection<ProcessedTarget>> currentTargets = new ConcurrentHashMap<>();
		
	public DetectionProcessing() {
	}
	
	public void start() {
	}
	
	public void stop() {
	}
	
	private void updateTargets(Thread thread, Collection<Detection> detections) {
		long processTime = (new Date()).getTime();
		

		List<ProcessedTarget> targets =  new ArrayList<>(detections.size());
		
		for (Detection d : detections) {
			Target t = d.getSimulated();
			if (t != null) {
				ProcessedTarget processed = new ProcessedTarget(t.getId(), t.getLocation(), t.getSpeed(),
						t.getHeading(), processTime);
				targets.add(processed);
			}
		}
			
		this.currentTargets.put(thread, targets);
	}
	
	public void addDetectionProvider(DetectionProvider provider) {
		System.out.println("Add DectectionProvider");
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (!Thread.interrupted()) {
					try {
						Collection<Detection> detections = provider.getDetections();
						updateTargets(Thread.currentThread(), detections);
					} catch (Exception e) {
						currentTargets.remove(Thread.currentThread());
					}
					try {
						Thread.sleep(TIMEOUT_IN_MS);
					} catch (InterruptedException ie) {
						break;
					}
				}
			}
		});
		this.providers.put(thread, provider);
		thread.start();
	}
	
	public void removeDetectionProvider(DetectionProvider provider) {
		for (Entry<Thread, DetectionProvider> entry : this.providers.entrySet()) {
			if (entry.getValue() == provider) {
				System.out.println("Remove DectectionProvider");
				this.providers.remove(entry.getKey());
				this.currentTargets.remove(entry.getKey());
				Thread.interrupted();
				break;
			}
		}
	}
	
	@Override
	public Collection<ProcessedTarget> getTargets() {
		Collection<ProcessedTarget> targets = new LinkedList<>();
		for (Collection<ProcessedTarget> subCol : this.currentTargets.values()) {
			targets.addAll(subCol);
		}
		return targets;
	}
}

package org.inaetics.apachecon.api.provider;

import java.util.List;

import org.inaetics.apachecon.api.data.Detection;

public interface DetectionProvider {

	List<Detection> getDetections();
	
}

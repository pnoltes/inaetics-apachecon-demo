package org.inaetics.apachecon.api.provider;

import java.util.Collection;

import org.inaetics.apachecon.api.data.ProcessedTarget;

public interface ProcessedTargetProvider {

	public Collection<ProcessedTarget> getTargets();
	
}

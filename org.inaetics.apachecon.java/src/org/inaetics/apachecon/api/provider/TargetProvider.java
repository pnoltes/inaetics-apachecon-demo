package org.inaetics.apachecon.api.provider;

import java.util.List;

import org.inaetics.apachecon.api.data.Target;

public interface TargetProvider {

	public List<Target> getTargets();
	
}

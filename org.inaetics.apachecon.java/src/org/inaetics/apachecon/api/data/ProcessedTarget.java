package org.inaetics.apachecon.api.data;

import org.inaetics.apachecon.api.util.LocationUtils;

public class ProcessedTarget extends Target {

	private long processTime;	
	
	public ProcessedTarget() {
		this(0,null, 0.0, 0.0, 0);
	}
	
	public ProcessedTarget(long id, Location location, double speed, double heading, long processTime) {
		super(id, location, speed, heading);
		this.processTime = processTime;
	}

	public long getProcessTime() {
		return processTime;
	}

	public void setProcessTime(long processTime) {
		this.processTime = processTime;
	}

	
	public Location getPrediction() {
		return getLocation() == null ? null : 
			LocationUtils.move(getLocation(), getHeading(), getSpeed() * 10);
	}

	public void setPrediction(Location loc) {
		//ignore
	}
}

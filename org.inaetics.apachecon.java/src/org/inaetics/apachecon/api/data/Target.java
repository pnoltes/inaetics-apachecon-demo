package org.inaetics.apachecon.api.data;

import java.util.Date;

public class Target {

	private long id;
	private Location location;
	private double speed; //m/s
	private double heading; //degrees
	private Long lastUpdated; //ms epoch
	
	
	public Target() {
		this(0, null,0.0, 0.0);
	}
	
	public Target(long id, Location location, double speed, double heading) {
		super();
		this.id = id;
		//this.source = source;
		this.location = location;
		this.speed = speed;
		this.heading = heading;
		this.lastUpdated = (new Date()).getTime();
	}
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}
	
	public double getSpeed() {
		return speed;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Long getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Long lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	public double getHeading() {
		return heading;
	}

	public void setHeading(double heading) {
		this.heading = heading;
	}
}

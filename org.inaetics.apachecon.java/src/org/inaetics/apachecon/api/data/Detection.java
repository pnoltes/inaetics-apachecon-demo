package org.inaetics.apachecon.api.data;

public class Detection {

	private long id;
	private Location center;
	private double range;
	private Target simulated = null; //set when detection if from a simulated source
	
	public Detection() {
		this(0, null,0.0);
	}
	
	public Detection(long id, Location center, double range) {
		super();
		this.id = id;
		this.center = center;
		this.range = range;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Location getCenter() {
		return center;
	}

	public void setCenter(Location center) {
		this.center = center;
	}

	public double getRange() {
		return range;
	}

	public void setRange(double range) {
		this.range = range;
	}

	public Target getSimulated() {
		return simulated;
	}

	public void setSimulated(Target simulated) {
		this.simulated = simulated;
	}
}

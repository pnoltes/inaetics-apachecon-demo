package org.inaetics.apachecon.api.util;

import org.inaetics.apachecon.api.data.Location;

import static java.lang.Math.*;

public abstract class LocationUtils {

	public static double distance(Location loc1, Location loc2) {
		double theta = toRadians(loc1.getLon() - loc2.getLon());
		//double lon1 = Math.toRadians(loc1.getLon());
		double lat1 = toRadians(loc1.getLat());
		//double lon2 = Math.toRadians(loc2.getLon());
		double lat2 = toRadians(loc2.getLat());
		
		double dist = sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(theta);
		dist = acos(dist);
		dist = toDegrees(dist);
		dist = dist * 60 * 1.1515 * 1.609344 * 1000;

		return dist;
	}

	public static Location move(Location start, double heading, double dist) {
	    
	    double lat1 = toRadians(start.getLat());
	    double lon1 = toRadians(start.getLon());
	    double angle = toRadians(heading);
	    
	    double dist_radians = toRadians(dist / (60 * 1.1515 * 1.609344 * 1000));
	    double lat = asin(sin(lat1) * cos(dist_radians) + cos(lat1) * sin(dist_radians) * cos(angle));
	    double dlon = atan2(sin(angle) * sin(dist_radians) * cos(lat1), cos(dist_radians) - sin(lat1) * sin(lat));
	    double lon = ((lon1-dlon + PI) % (2*PI)) - PI;
	    
	    Location result = new Location(toDegrees(lat), toDegrees(lon));
	    return result;
	}
	
	public static Location moveDelta(Location start, double deltaX, double deltaY) {
		double angle = atan2(-1 * deltaX, -1 * deltaY); // In radians
		double dist = sqrt(deltaX*deltaX + deltaY*deltaY);
		return LocationUtils.move(start, toDegrees(angle), dist);
	}

}
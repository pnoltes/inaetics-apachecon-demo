package org.inaetics.apachecon.api.sensor;

import org.inaetics.apachecon.api.data.Location;

public interface Sensor {

	String getName();
	Location getLocation();
	Double getRange();

}

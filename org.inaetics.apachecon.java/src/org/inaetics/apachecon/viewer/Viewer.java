package org.inaetics.apachecon.viewer;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.inaetics.apachecon.api.data.Detection;
import org.inaetics.apachecon.api.data.Location;
import org.inaetics.apachecon.api.data.ProcessedTarget;
import org.inaetics.apachecon.api.data.Target;
import org.inaetics.apachecon.api.provider.DetectionProvider;
import org.inaetics.apachecon.api.provider.ProcessedTargetProvider;
import org.inaetics.apachecon.api.provider.TargetProvider;
import org.inaetics.apachecon.api.sensor.Sensor;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;



public class Viewer extends HttpServlet {

	private static final long serialVersionUID = 2327555446186210177L;
	private static final long TIMEOUT_IN_MS = 1000;
	private static final long SENSOR_TIMEOUT_IN_MS = 5000;
	
	private volatile HttpService httpService;
	
	private volatile TargetProvider simulated;
	private volatile ProcessedTargetProvider processedProvider;
		
	private final JsonFactory factory = new JsonFactory();
	private final ObjectMapper mapper = new ObjectMapper();
	private final ObjectWriter sensorWriter = mapper.writerFor(Sensor.class);
	private final ObjectWriter detectionWriter = mapper.writerFor(Detection.class);
	private final ObjectWriter targetWriter = mapper.writerFor(Target.class);
	private final ObjectWriter processedTargetWriter = mapper.writerFor(ProcessedTarget.class);
	
	private final Thread updateTargetsThread;
	private final Thread updateProcessedThread;
	
	private final Map<Thread, Sensor> sensors = new ConcurrentHashMap<>();
	private final Map<Thread, Sensor> latestSensors = new ConcurrentHashMap<>();
	
	private final Map<Thread, DetectionProvider> detectionProviders = new ConcurrentHashMap<>();
	private final Map<Thread, Collection<Detection>> latestDetections = new ConcurrentHashMap<>();
	
	private volatile Collection<Target> latestTargets = new ArrayList<>();
	private volatile Collection<ProcessedTarget> latestProcessed = new ArrayList<>();
	
	private class SensorCopy implements Sensor {
		
		private final String name;
		private final Location location;
		private final Double range;
		
		public SensorCopy(String name, Sensor sensor) {
			this.name = name;
			this.location = sensor.getLocation();
			this.range = sensor.getRange();
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public Location getLocation() {
			return location;
		}

		@Override
		public Double getRange() {
			return range;
		}
		
	}
	
	public Viewer() {
		this.updateTargetsThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (!Thread.interrupted()) {
					try {
						updateTargets();
						Thread.sleep(TIMEOUT_IN_MS);
					} catch (InterruptedException e) {
						break;
					}
				} 
			}
		}, "viewer-update-targets-thread");
		this.updateProcessedThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (!Thread.interrupted()) {
					try {
						updateProcessed();
						Thread.sleep(TIMEOUT_IN_MS);
					} catch (InterruptedException e) {
						break;
					}
				} 
			}
		}, "viewer-update-processed-thread");
	}
	
	public void start() {
		updateTargetsThread.start();
		updateProcessedThread.start();
		
		try {
			httpService.registerServlet("/", this, null, null);
		} catch (ServletException | NamespaceException e) {
			e.printStackTrace();
		}
	}
	
	public void stop() {
		updateTargetsThread.interrupt();
		updateProcessedThread.interrupt();
		
		httpService.unregister("/");
	}
	
	private void updateProcessed() {
		Collection<ProcessedTarget> targets = null;
		try {
			targets = this.processedProvider.getTargets();
		} catch (Exception e) {
			System.err.println("error invoking processed target provider");
		}
		targets = targets == null ? new LinkedList<>() : targets;
		this.latestProcessed = targets;
	}

	private void updateTargets() {
		List<Target> targets = null;
		try {
			targets = simulated.getTargets();
		} catch (Exception e) { 
			System.err.println("error invoking target provider");
		}
		if (targets == null) {
			targets = new ArrayList<>();
		}
		this.latestTargets = targets;
	}

	public void addSensor(Sensor sensor) {
		System.out.println("adding sensor");
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (!Thread.interrupted()) {
					try { 
						String name = sensor.getName();
						SensorCopy copy = new SensorCopy(name, sensor);
						latestSensors.put(Thread.currentThread(), copy);
					} catch (Exception e) {
						System.err.println("Error invoking sensor");
						latestSensors.remove(Thread.currentThread());
					}
					try {
						Thread.sleep(SENSOR_TIMEOUT_IN_MS);
					} catch (InterruptedException ie) {
						break;
					}
				}
			}
		});
		sensors.put(thread, sensor);
		thread.start();
	}
	
	public void removeSensor(Sensor sensor) {
		for (Entry<Thread, Sensor> entry : this.sensors.entrySet()) {
			if (entry.getValue() == sensor) {
				System.out.println("removing sensor");
				entry.getKey().interrupt();
				sensors.remove(entry.getKey());
				latestSensors.remove(entry.getKey());
				break;
			}
		}
	}
	
	public void addDetectionProvider(DetectionProvider provider) {
		System.out.println("adding detection provider");
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (!Thread.interrupted()) {
					try { 
						Collection<Detection> detections = provider.getDetections();
						latestDetections.put(Thread.currentThread(),  detections);
					} catch (Exception e) {
						System.err.println("Error invoking sensor");
						latestDetections.remove(Thread.currentThread());
					}
					try {
						Thread.sleep(TIMEOUT_IN_MS);
					} catch (InterruptedException ie) {
						break;
					}
				}
			}
		});
		detectionProviders.put(thread, provider);
		thread.start();
	}
	
	public void removeDetectionProvider(DetectionProvider provider) {
		for (Entry<Thread, DetectionProvider> entry : this.detectionProviders.entrySet()) {
			if (entry.getValue() == provider) {
				System.out.println("removing sensor");
				entry.getKey().interrupt();
				detectionProviders.remove(entry.getKey());
				latestDetections.remove(entry.getKey());
				break;
			}
		}	
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String path = req.getPathInfo();
		JsonGenerator gen = factory.createGenerator(resp.getOutputStream());
		//System.out.println("path is " + path);
		
		if (path == null || "/".equals(path)) {
			path = "index.html";
		}
		
		InputStream resource = getClass().getClassLoader().getResourceAsStream(path);
		
		if (resource != null) { 
			byte[] buffer = new byte[1024];
			int len;
			while ((len = resource.read(buffer)) != -1) {
			    resp.getOutputStream().write(buffer, 0, len);
			}
		} else if ("/sensors".equals(path)) {
			resp.setContentType("application/json");
			gen.writeStartArray();
			for (Sensor s : this.latestSensors.values()) {
				sensorWriter.writeValue(gen,  s);
			}
			gen.writeEndArray();
			gen.flush();
		} else if ("/detections".equals(path)) {
			resp.setContentType("application/json");
			gen.writeStartArray();
			for (Collection<Detection> collection : this.latestDetections.values()) {
				for (Detection d : collection) {
					detectionWriter.writeValue(gen,  d);
				}
			}
			gen.writeEndArray();
			gen.flush();
		} else if ("/simulated".equals(path)) {
			resp.setContentType("application/json");
			gen.writeStartArray();
			for (Target t : this.latestTargets) {
				targetWriter.writeValue(gen, t);
			}
			gen.writeEndArray();
			gen.flush();
		} else if ("/processed".equals(path)) {
			resp.setContentType("application/json");
			gen.writeStartArray();
			for (ProcessedTarget t : this.latestProcessed) {
				processedTargetWriter.writeValue(gen, t);
			}
			gen.writeEndArray();
			gen.flush();
		} else {
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
		
	} 
	
	

}

/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.inaetics.apachecon.viewer;

import java.util.Properties;

import javax.servlet.Servlet;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.inaetics.apachecon.api.provider.DetectionProvider;
import org.inaetics.apachecon.api.provider.ProcessedTargetProvider;
import org.inaetics.apachecon.api.provider.TargetProvider;
import org.inaetics.apachecon.api.sensor.Sensor;
import org.osgi.framework.BundleContext;
import org.osgi.service.http.HttpService;

public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        Properties props = new Properties();
        //props.put("alias", "/map");
        //props.put("init.message", "Hello World!");
        
        manager.add(createComponent()
            .setInterface(Servlet.class.getName(), props)
            .setImplementation(Viewer.class)
            .add(createServiceDependency().setService(HttpService.class).setRequired(true))
            .add(createServiceDependency().setService(Sensor.class)
            		.setCallbacks("addSensor", "removeSensor"))
            .add(createServiceDependency().setService(DetectionProvider.class)
            		.setCallbacks("addDetectionProvider", "removeDetectionProvider"))
            .add(createServiceDependency().setService(TargetProvider.class))
            .add(createServiceDependency().setService(ProcessedTargetProvider.class))
        );
    }
    
    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {

    }

}

package org.inaetics.apachecon.simulator;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.inaetics.apachecon.api.data.Location;
import org.inaetics.apachecon.api.data.Target;
import org.inaetics.apachecon.api.provider.TargetProvider;
import org.inaetics.apachecon.api.util.LocationUtils;

public class TargetSimulator implements TargetProvider {
	
	public static final String SOURCE = "simulated";
	private static final int MAX_TARGETS = 1;
	private static final long TIMEOUT_IN_MS = 200;

	private final Location targetCenter = new Location(47.502657, 19.066798);
	private final double maxRange = 200;
	private final Thread thread;
	
	private AtomicLong nextId = new AtomicLong(1);
	private volatile List<Target> targets = new LinkedList<Target>();
	
	public TargetSimulator() {
		thread = new Thread(new Runnable() {
			@Override
			public void run() {
				while(!Thread.interrupted()) {
					try {
						updateTargets();
						Thread.sleep(TIMEOUT_IN_MS);
					} catch (InterruptedException e) {
						break;
					}
				}
			}
		}, "Target Simulator");
	}
	
	public void start() {
		thread.start();
	}
	
	public void stop() {
		System.out.println("interrupting target simulator");
		thread.interrupt();
	}
	

	@Override
	public List<Target> getTargets() {
		return targets;
	}
	
	
	public void updateTargets() {
		List<Target> newTargets = new LinkedList<Target>();
		long updateTime = (new Date()).getTime();
		
		for (Target t : targets) {
			moveTarget(t, updateTime);
			double dist = LocationUtils.distance(t.getLocation(), this.targetCenter);
			if (dist <= this.maxRange) {
				t.setLastUpdated(updateTime);
				newTargets.add(t);
			}
		}	
		
		if (newTargets.size() < MAX_TARGETS) {
			int diff = MAX_TARGETS - newTargets.size();
			for (int i = 0; i < diff; i += 1) {
				Target t = spawnTarget(updateTime);
				newTargets.add(t);
			}
		}
		
		this.targets = newTargets;
		//System.out.println("updated targets");
	}
	
	private Target spawnTarget(long updateTime) {
		Location loc = LocationUtils.move(this.targetCenter, Math.random() * 360, Math.random() * this.maxRange);
		double speed = Math.random() * 5 + 1;
		double heading = Math.random() * 360;
		Target target = new Target(nextId.getAndIncrement(), loc, speed, heading);
		return target;
	}

	private void moveTarget(Target target, long forTime) {
		long t1 = target.getLastUpdated();
		long t2 = forTime;
		double elapsed = t2 - t1;
		elapsed = elapsed > 0 ? elapsed : 0;
		elapsed = elapsed / 1000; //in second
		
		double dist = target.getSpeed() * elapsed;
		
		Location newLoc = LocationUtils.move(target.getLocation(), target.getHeading(), dist);
		//System.out.println("Moved target from lat " + target.getLocation().getLat() + " to " + newLoc.getLat());
		//System.out.println("Moved target from lon " + target.getLocation().getLon() + " to " + newLoc.getLon());
		target.setLocation(newLoc);
	}
}

package org.inaetics.apachecon.simulator;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.inaetics.apachecon.api.provider.TargetProvider;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
    	Properties props = new Properties();
        props.put("source", TargetSimulator.SOURCE);
    	props.put("service.exported.interfaces", TargetProvider.class.getName());
    	
    	manager.add(createComponent()
                .setInterface(TargetProvider.class.getName(), props)
                .setImplementation(TargetSimulator.class)
        );
    }
    
    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {

    }

}

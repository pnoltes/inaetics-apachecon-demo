#INAETICS ApacheCon Core Demonstrator

##TODO

1. ~~ replace node-agent-java with inaetis node-agent-java (rsa http based) - Bjorn ~~
1. ~~ Finish C sensor so that is can be used for 1 sensor in the first demo and two sensor groups in the self healing demo - Pepijn ~~
1. ~~ Add a Java Detection Processor so that it will report targets within it's area. Note that the detection already contains the simulated targets. Only needed in the self healing demo - Pepijn ~~
1. Prepare felix with viewer, simululator & sensor located round apache con hotel - Pepijn
1. Prepare 2nd felix with sensor located near the other sensor - Pepijn 
1. Prepare celix with sensor also located near the org. sensor - Pepijn
1. Optional Prepare 4th felix with can be provisioned by ACE
1. Optional Prepare ace with a 4th sensor located near the org. sensor
1. ~~Push TNL-TNO demonstrator cluster setup (without the docker results) - Pepijn ~~
1. ~~ Adjust the cluster setup to that node-agent-c is build with ffi - Pepijn ~~
1. ~~ Adjust the viewer (index.html) so that it is centered @ hotel apachecon - Bjorn ~~
1. Adjust the cluster setup to that the provision is started with the following targets sensor-group-1, sensor-group-2, dectection-processing and viewer - Bjorn
1. ~~ Adjust the cluster setup to that the provision are outside the docker image.  - Bjorn ~~
1. ~~ Adjust the cluster setup to that celix@ and felix@ are not getting an ip id, but the instance id provided when using fleetctl - Both ~~
1. ~~ Adjust the init-working to that only node-agent-c, node-agent-java and provisioning is loaded. - Both ~~
1. Adjust Viewer, processor so that there is a thread per provider. This would be better to handle timeout connection and keep the appl snappy
1. Fix issue with java rsa. The java rsa seems to have a bug concerning removing of remote services. Why kill a service this is not always picked up by the service user.

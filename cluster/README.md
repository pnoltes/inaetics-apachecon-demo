# Building local applications
This builds the applications whiche are running locally (i.e. not in a docker container)
1. ``cd <demonstrator-root>/demonstrator``
2. ``mkdir build``
3. ``cd build``
4. ``ccmake ..``
5. ``make deploy-all``

# Building docker containers
This (rebuilds the docker containers. During the build the software inside the containers is rebuild.
1. ``cd <demonstrator-root>``
2. ``./create_images.sh``
3. or 
4. ``./create_images.sh <image name> # This builds only the given image``

# Starting Ineatics software
1. ``cd <demonstrator-root>/workers``
2. vagrant up (if vagrant is already running stop this first with vagrant destroy -f)
3. Wait until the VMs are started.
4. vagrant ssh worker-1
5. Check  if everything is started with:
        ``systemctl --failed``
6. If needed start ineartics service:
        ``sudo systemctl restart inaetics``
7. ``exit`` (exits back to the host computer)

# Starting demonstrator SW
1. Open an xterm
2. ``cd <demonstrator-root>/demonstrator/build/deploy/target-generator``
3. ``sh run.sh``
4. Open an xterm
5. ``cd <demonstrator-root>/demonstrator/build/deploy/tda`
6. ``sh run.sh``
7. Open an xterm
8. ``cd <demonstrator-root>/demonstrator/build/deploy/reasoner`
9. ``sh run.sh``






 




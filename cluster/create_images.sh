if [ "$#" -ne "0" ]
then
	IMAGES=$*
else
	IMAGES=`ls docker-images`
fi
for im in $IMAGES
do
	echo "Building image $im"
	docker build -t $im docker-images/$im
	echo "Saving image $im"
	docker save $im | gzip -c - > workers/inaetics/docker/$im.tar.gz
done

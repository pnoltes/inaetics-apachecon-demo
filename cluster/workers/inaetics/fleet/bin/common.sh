#!/bin/sh

#IP
SUBNET_INTERFACE=192.168.1.
MY_IP=$(ip a | grep ${SUBNET_INTERFACE} | grep inet\ | awk '{print $2}'| cut -f1 -d'/')


#ETCD
ETCD_CLIENT_PORT=4001
ETCD_PEER_PORT=7001
ETCDCTL_PEERS=${MY_IP}:${ETCD_CLIENT_PORT}

#DOCKER
#DEFAULT_REPOSITORY_DOCKER_HOST=172.17.8.2
DEFAULT_REPOSITORY_DOCKER_HOST=192.168.1.10
DEFAULT_REPOSITORY_DOCKER_PORT=5000

DOCKER_REPOSITORY_HOST=$(etcdctl get /inaetics/docker/repository/host 2> /dev/null || /bin/echo ${DEFAULT_REPOSITORY_DOCKER_HOST})
DOCKER_REPOSITORY_PORT=$(etcdctl get /inaetics/docker/repository/port 2> /dev/null || /bin/echo ${DEFAULT_REPOSITORY_DOCKER_PORT})

# Script configuration
VERBOSE_LOGGING=true
RETRY_INTERVAL=30
UPDATE_INTERVAL=300

HOSTNAME=""
MACHINE_ID=""
INSTANCE_ID=""
COMMAND=""

function parse_args {
        for ARG in $*; do
                case ${ARG} in
                        --stop)
                                COMMAND="stop"
                                ;;
                        --start)
                                COMMAND="start" 
                                ;;
                        --hostname=*)
                                HOSTNAME=`echo ${ARG} | cut -d"=" -f2`
                                ;;
                        --machineId=*)
                                MACHINE_ID=`echo ${ARG} | cut -d"=" -f2`
                                ;;
                        --instanceId=*)
                                INSTANCE_ID=`echo ${ARG} | cut -d"=" -f2`
                                ;;
                        *)
                                echo "Unknown argument ${ARG}"
                                ;;
                esac
        done

        if [[ -z ${HOSTNAME} || -z ${MACHINE_ID} || -z ${COMMAND} || -z ${INSTANCE_ID} ]] 
        then
                echo "Usage $0 --hostname=<hostname> --machineId=<machineid> --instanceId=<instanceid> (--start | --stop)"
                exit 1
        fi

}


function remove_docker_container {
        IMG_NAME=$1
        GOT_IMG=$(/usr/bin/docker ps -a | grep ${IMG_NAME})
        if [ -n "${GOT_IMG}" ] 
        then
                /usr/bin/docker rm -f ${IMG_NAME}
        fi
}

function docker_image_load {
        IMG_NAME=$1
        IMG_URL=$2
        GOT_IMG=$(/usr/bin/docker images | grep ${IMG_NAME})
        echo "IMG_NAME is ${IMG_NAME}"
        echo "IMG_URL is ${IMG_URL}"
        echo "GOT_IMG is ${GOT_IMG}"

        if [ -n "${GOT_IMG}" ] 
        then
                echo "${IMG_NAME} already loaded"
        else
                echo "Downloading and loading ${IMG_NAME} @ ${IMG_URL} ..."
                curl -s -L ${IMG_URL} | gzip -d -c - | docker load
        fi
}

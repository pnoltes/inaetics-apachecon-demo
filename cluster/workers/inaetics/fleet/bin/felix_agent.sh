#!/bin/sh

source /opt/inaetics/fleet/bin/common.sh
parse_args $*

#USE_IP_BASED_ID=false
MY_ID=$(echo $MY_IP | tr -d '.')
DOCKER_NAME="felix-${INSTANCE_ID}"
if [ ${USE_IP_BASED_ID} ] 
then
  DOCKER_NAME="felix_${MY_ID}"
fi

#DOCKER_IMG=${DOCKER_REPOSITORY_HOST}:${DOCKER_REPOSITORY_PORT}/stars-node-agent:latest
DOCKER_IMG=node-agent-java

PORTS=""
for ((i=50000 ; i<50100; i++)) 
do
        PORTS="${PORTS} -p $i:$i"
done

if [ ${COMMAND} = "start" ] 
then
	remove_docker_container ${DOCKER_NAME}
	/usr/bin/docker run --rm=true --hostname="felix-${HOSTNAME}" --name="${DOCKER_NAME}" -p 6661:6666 -p 8081:8080 -p 9991:9999 -p 8881:8888 -e ETCDCTL_PEERS=${ETCDCTL_PEERS} ${DOCKER_IMG} /tmp/node-agent.sh ${DOCKER_NAME} $MY_IP
else

	/usr/bin/docker stop ${DOCKER_NAME}
	remove_docker_container ${DOCKER_NAME}
fi


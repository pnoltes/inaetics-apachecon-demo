#!/bin/sh

source /opt/inaetics/fleet/bin/common.sh
parse_args $*

#USE_IP_BASED_ID=false
MY_ID=$(echo $MY_IP | tr -d '.')
DOCKER_NAME="celix-${INSTANCE_ID}"
if [ ${USE_IP_BASED_ID} ] 
then
  DOCKER_NAME="celix_${MY_ID}"
fi

#DOCKER_IMG=${DOCKER_REPOSITORY_HOST}:${DOCKER_REPOSITORY_PORT}/stars-node-agent:latest
DOCKER_IMG=node-agent-c

PORTS=""
#Needed for pubsub with zmq
#for ((i=40000 ; i<40100; i++)) 
#do
#        PORTS="${PORTS} -p $i:$i"
#done

if [ ${COMMAND} = "start" ] 
then
	remove_docker_container ${DOCKER_NAME}
	/usr/bin/docker run --rm=true --hostname="celix-${HOSTNAME}" --name="${DOCKER_NAME}" -p 6668:6666 -p 9999:9999 -p 8888:8888 ${PORTS} -e ETCDCTL_PEERS=${ETCDCTL_PEERS} ${DOCKER_IMG} /tmp/node-agent.sh ${DOCKER_NAME} $MY_IP
else
	/usr/bin/docker stop ${DOCKER_NAME}
	remove_docker_container ${DOCKER_NAME}
fi


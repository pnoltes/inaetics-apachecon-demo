#!/bin/sh

for ARG in $*
do
        DOCKER_LOC=${ARG}
        echo "Downloading ${DOCKER_LOC} and loading it to docker"
        #        /bin/curl -N -# -L ${DOCKER_URL} | /usr/bin/gzip -d -c - | /usr/bin/docker load && echo "Loaded ${DOCKER_URL} to docker"
        gzip -d -c ${DOCKER_LOC} | /usr/bin/docker load && echo "Loaded ${DOCKER_LOC} to docker"
done

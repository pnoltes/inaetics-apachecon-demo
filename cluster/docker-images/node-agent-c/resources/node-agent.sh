#!/bin/bash
#
# Start scrip for the Node Agent
#
# (C) 2014 INAETICS, <www.inaetics.org> - Apache License v2.

#TODO the workdir should be removed when stopping the agent

cd $(dirname $0)

#
# Config
#
PROVISIONING_NAMESPACE="/inaetics/node-provisioning-service"
MAX_RETRY_ETCD_REPO=10
RETRY_ETCD_REPO_INTERVAL=5
UPDATE_INTERVAL=20
RETRY_INTERVAL=20
ETCD_TTL_INTERVALL=$((UPDATE_INTERVAL + 15))
LOG_DEBUG=true

#
# Libs
#
source etcdctl.sh

# Wraps a function call to redirect or filter stdout/stderr
# depending on the debug setting
#   args: $@ - the wrapped call
#   return: the wrapped call's return
_call () {
  if [ "$LOG_DEBUG" != "true"  ]; then
    $@ &> /dev/null
    return $?
  else
    $@ 2>&1 | awk '{print "[DEBUG] "$0}' >&2
    return ${PIPESTATUS[0]}
  fi
}

# Echo a debug message to stderr, perpending each line
# with a debug prefix.
#   args: $@ - the echo args
_dbg() {
  if [ "$LOG_DEBUG" == "true" ]; then
    echo $@ | awk '{print "[DEBUG] "$0}' >&2
  fi
}

# Echo a log message to stderr, perpending each line
# with a info prefix.
#   args: $@ - the echo args
_log() {
  echo $@ | awk '{print "[INFO] "$0}' >&2
}


#
# State
#
current_provisioning_location=""
agent_pid=""

#
# Functions
#

# Locate the provisioning service in etcd.
#  echo: <new service>, may be same as current
#  return: 0, if no errors
#    1, if etcd lookup fails
locate_provisioning_service () {
  local located_provisioning_service=""
  local provisioning_services=($(etcd/values $PROVISIONING_NAMESPACE $ETCD_HOST))
  if [ $? -ne 0 ]; then
    return 1
  fi
  if [ ${#provisioning_services[@]} -gt 0 ]; then
    located_provisioning_service=${provisioning_services[0]}
  fi
  echo "${located_provisioning_service}"
  return 0
}

start_agent () {
  DEPLOYMENT_ID=${agent_id}
  HOST_IP=${agent_ipv4}
  MAX_RETRY_ETCD_REPO=60
  RETRY_ETCD_REPO_INTERVAL=5
  DISCOVERY_PATH="org.apache.celix.discovery.etcd"

  local workdir="/tmp/workdir"
  mkdir -p ${workdir}

  cp /tmp/config.properties.base ${workdir}/config.properties
  echo "deployment_admin_identification=${agent_id}" >> ${workdir}/config.properties
  echo "deployment_admin_url=${current_provisioning_location}" >> ${workdir}/config.properties
  echo "RSA_IP=$agent_ipv4" >> ${workdir}/config.properties
  echo "DISCOVERY_ETCD_ROOT_PATH=inaetics/discovery" >> ${workdir}/config.properties
  echo "DISCOVERY_ETCD_SERVER_IP=`echo $ETCDCTL_PEERS | cut -d ':' -f 1`" >> ${workdir}/config.properties
  echo "DISCOVERY_ETCD_SERVER_PORT=`echo $ETCDCTL_PEERS | cut -d ':' -f 2`" >> ${workdir}/config.properties
  echo "DISCOVERY_CFG_SERVER_IP=$agent_ipv4" >> ${workdir}/config.properties

  cd ${workdir}
  local cmd="celix"

  _dbg $cmd
  $cmd 
}

function store_etcd_data(){

  # check if provisioning is running
  if [ "$agent_pid" == "" ]; then
  	_log "service not running, skipping store_etcd_data"
    return
  fi

  ETCD_PATH_FOUND=0
  RETRY=1
  while [ $RETRY -le $MAX_RETRY_ETCD_REPO ] && [ $ETCD_PATH_FOUND -eq 0 ]
  do
    etcd/putTtl "/inaetics/node-agent-service/$agent_id" "$agent_ipv4:$agent_port" "$ETCD_TTL_INTERVALL"

    if [ $? -ne 0 ]; then
        _log "Tentative $RETRY of storing agent to etcd failed. Retrying..."
        ((RETRY+=1))
        sleep $RETRY_ETCD_REPO_INTERVAL
    else
        _log "Pair </inaetics/node-agent-service/$agent_id,$agent_ipv4:$agent_port> stored in etcd"
        ETCD_PATH_FOUND=1
    fi
  done

  if [ $ETCD_PATH_FOUND -eq 0 ]; then
    _log "Cannot store pair </inaetics/node-agent-service/$agent_id,$agent_ipv4:$agent_port> in etcd"
  fi

}

stop_agent () {
    echo "Running cleanup.."
    etcd/rm "/inaetics/node-agent-service/$agent_id"

    celix_pid=`pidof celix`
    kill ${celix_pid}
}

check_provisioning_location() {
  while true; do
      sleep $UPDATE_INTERVAL
      local provisioning_location=`locate_provisioning_service`
      if [ "${provisioning_location}" == "" ]
      then
          echo "Locating provisioning service in etcd failed. Keeping current '${current_provisioning_location}'"
      elif [ "${provisioning_location}" != "${current_provisioning_location}" ]
      then
          echo "Provisioning location changed ('${current_provisioning_location}' -> '${provisioning_location}')"
          echo "Stopping"
          stop_agent
          break
      fi
  done
}


#
# Main
#
trap stop_agent SIGHUP SIGINT SIGTERM

agent_id=$1
if [ "$agent_id" == "" ]; then
  # get id from env variable set by kubernetes pod config
  agent_id=$AGENT_NAME
  if [ "$agent_id" != "" ]; then
  	# append ip
    agent_id=$agent_id-`hostname -i`
  fi
fi
if [ "$agent_id" == "" ]; then
  # get docker id
  agent_id=`cat /proc/self/cgroup | grep -o  -e "docker-.*.scope" | head -n 1 | sed "s/docker-\(.*\).scope/\\1/"`
fi
if [ "$agent_id" == "" ]; then
  echo "agent_id param required!"
  exit 1
fi

agent_ipv4=$2
if [ "$agent_ipv4" == "" ]; then
  # get IP 
  agent_ipv4=`hostname -i`
fi
if [ "$agent_ipv4" == "" ]; then
  echo "agent_ipv4 param required!"
  exit 1
fi

# get port from env variable set by kubernetes pod config
agent_port=$HOSTPORT
if [ "$agent_port" == "" ]; then
  agent_port=8080
fi

current_provisioning_location=`locate_provisioning_service`
check_provisioning_location &
start_agent
